#!/usr/bin/env node

const Fasti = require('fastify')
const cors = require('cors')
const jwt = require('fastify-jwt')
const auth = require('fastify-auth')
const utils = require('./utils')
const config = require('./config.json')

const v1 = require('./v1')

const PORT = process.env.PORT || 8080

const fastify = Fasti()

fastify
  .use(cors())
  .register(jwt, { secret: config.secret })
  .register(auth)
  .register(v1, { prefix: '/v1' })
//  .register(require('./v2'), { prefix: '/v2' })
  .decorate('verifyJWT', verifyJWT)
  .listen(PORT, (err) => {
    if (err) {
      throw err
    }
    utils.log(`listen on ${PORT}`) // eslint-disable-line no-console
  })

module.exports = fastify

fastify.addHook('onRequest', (req, res, next) => {
  utils.log(JSON.stringify({
    method: req.method,
    url: req.url
  }))
  next()
})

fastify.addHook('onSend', (request, reply, payload, next) => {
  utils.log(JSON.stringify({
    req: {
      method: request.req.method,
      url: request.req.url,
      headers: request.headers,
      query: request.query,
      body: request.body
    },
    res: {
      code: reply.res.statusCode,
      headers: reply.res._headers
    },
    payload
  }))
  next()
})

function verifyJWT(request, reply, done) {
  const jwt = this.jwt

  if (!request.req.headers['authorization']) {
    return done(new Error('Missing token header'))
  }

  const parts = request.req.headers['authorization'].split(' ')
  if (parts.length != 2 || parts[0] != 'Bearer' || parts[1].trim() == '') {
    return done(new Error('jwt malformed, ie: Authorization: Bearer token'))
  }

  jwt.verify(parts[1], (err, decoded) => {
    if (err) {
      return done(err)
    }
    if (!fastify.jwtDecoded) {
      fastify.decorate('jwtDecoded', decoded)
    }
    done()
  })
}
