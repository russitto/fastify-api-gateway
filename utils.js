const fetch = require('node-fetch')

// simple & unified get + post
module.exports = { request, log, logErr }

function request(url, definition) {
  const method = definition.method.toUpperCase()
  if (method == 'GET') {
    return gett(url, definition)
  }
  return post(url, definition)
}

function gett(url, definition) {
  function innerGet(request, reply) {
    let query = '', newUrl = url
    for (const param in request.query) {
      query += param + '=' + request.query[param] + '&'
    }
    if (query != '') {
      query = query.substr(0, query.length-1)
      newUrl += '?' + query
    }

    return myFetch(newUrl, request, reply)
  }
  definition.handler = innerGet
  return definition
}

function post(url, definition) {
  function innerPost(request, reply) {
    return myFetch(url, request, reply, request.body)
  }
  definition.handler = innerPost
  return definition
}

function myFetch(url, request, reply, postObj = null) {
  let f, response
  const headers = request.headers
  // fix, host header => 404
  delete headers.host
  // fix, content-length header => timeout hangout
  delete headers['content-length']
  if (postObj) {
    f = fetch(url, {
      method: 'POST',
      headers,
      body: JSON.stringify(postObj)
    })
  } else {
    f = fetch(url, {headers})
  }
  return f.then(res => {
    response = res
    return res.json()
  }).then(json => {
    if (response.status != 200) {
      logErr(response.status, json)
    }
    for (const head in response.headers._headers) {
      if (head.indexOf('x-') == 0) {
        reply.header(head, response.headers._headers[head])
      }
    }
    return reply.code(response.status).send(json)
  }).catch(e => {
    logErr(e)
    if (response) {
      if (response.headers) {
        for (const head in response.headers._headers) {
          if (head.indexOf('x-') == 0) {
            reply.header(head, response.headers._headers[head])
          }
        }
      }
      return reply.code(response.status).send('Error ' + e)
    }
    reply.code(500).send('Error ' + e)
  })
}

/*
let j = {
  url: '/util/checkdb',
  method: 'GET',
  schema: {
    response: {
      '200': {
        description: 'Succesful response',
        type: 'object',
        properties: {
          alive: {
            type: 'boolean'
          }
        }
      }
    }
  }
}
*/

function log(obj) {
  return console.log(obj) // eslint-disable-line no-console
}

function logErr(obj) {
  return console.error(obj) // eslint-disable-line no-console
}
