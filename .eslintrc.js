module.exports = {
  "env": {
    "browser": false,
    "es6": true,
    "node": true
  },
  "parserOptions": {
    "ecmaVersion": 2017,
    "sourceType": "module"
  },
  "extends": "eslint:recommended",
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "single",
      { "allowTemplateLiterals": true }
    ],
    "semi": [
      "error",
      "never"
    ],
    "max-len": [
      "warn",
      120
    ],
    "no-console": "warn",
    "no-useless-escape": 0,
    "curly": "error",
    "prefer-const": "error"
  }
}
