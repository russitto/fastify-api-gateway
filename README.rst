.. -*- coding: utf-8 -*-

===================================
Node.js Rest API with JWT + Swagger
===================================

.. image:: doc/logo.png
  :scale: 80%

.. |date| date:: %d/%m/%Y %H:%M
.. |version| date:: %Y%m%d
.. contents:: Contenidos

.. header::

  ###Title### - Matias Russitto - mrussitto@qubit.tv

.. footer::

  ###Page### / ###Total###

.. raw:: pdf

  PageBreak

-----
Start
-----

.. code-block:: bash

  $ npm start

----------------
More controllers
----------------

Add controllers files in v1 dir (or v2, etc…), then edit v1/index.js

------------
Requirements
------------

.. code-block:: bash

  $ npm i -g bootprint bootprint-swagger eslint http-server

--------------------
Generate swagger doc
--------------------

.. code-block:: bash

  $ npm run doc && npm run host-doc
