#!/usr/bin/env node

const Fasti = require('fastify')
const cors = require('cors')
const swagger = require('fastify-swagger')
const jwt = require('fastify-jwt')
const auth = require('fastify-auth')
const util = require('util')
const pack = require('./package.json')

const v1 = require('./v1')
const _l = console.log.bind(console) // eslint-disable-line no-console

const fastify = Fasti({
  logger: true
})

fastify
  .use(cors())
  .register(jwt, { secret: 'sandia con vino' })
  .register(auth)
  .register(swagger, {
    swagger: {
      info: {
        title: pack.name,
        description: pack.description,
        version: pack.version
      },
      host: 'localhost',
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
      securityDefinitions: {
        Bearer: {
          description: 'authorization bearer',
          type: 'apiKey',
          name: 'Authorization',
          in: 'header'
        }
      }
    }
  })
  .register(v1, { prefix: '/v1' })
//  .register(require('./v2'), { prefix: '/v2' })
  .decorate('verifyJWT', verifyJWT)

  .ready(() => {
    const yaml = process.argv[2] && process.argv[2] == 'yaml'
    const doc = fastify.swagger({yaml})
    if (yaml) {
      return _l(doc) // 
    }
    _l(util.inspect(doc, false, null))
  })

function verifyJWT(request, reply, done) {
  done()
}
