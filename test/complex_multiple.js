const request = require('supertest')
const should = require('should')
const app = require('../app')
const _l = console.log.bind(console) // eslint-disable-line no-console

let token = ''
const r = request(app)

r.get('/v1/hello')
  .expect('Content-Type', 'application/json')
  .expect(200)
  .expect({
    hello: 'world'
  })
  .end((err) => {
    if (err) {
      throw err
    }
    // callAuth(r)
    call404(r)
  })

function call404(r) {
  r.get('/v1/sandia404')
    .expect(res => {
      should(res.statusCode).not.equal(200)
    })
    .expect(404)
    .end((err) => {
      if (err) {
        throw err
      }
      // callAuth(r)
      callAuth(r)
    })
}

function callAuth(r) {
  r.post('/v1/complex/auth')
    .send({operator: 'qubit'})
    .expect('Content-Type', 'application/json')
    .expect(200)
    .expect(res => {
      token = res.body
    })
    .end((err) => {
      if (err) {
        throw err
      }
      callMultiHello(r)
    })
}

function callMultiHello(r) {
  r.post('/v1/complex/hello')
    .set('Authorization', 'Bearer ' + token)
    .send({name: 'Matias'})
    .expect('Content-Type', 'application/json')
    .expect(200)
    .expect({
      hello: 'Matias @ qubit'
    })
    .expect(res => {
      _l(res.body)
    })
    .end((err) => {
      if (err) {
        throw err
      }
      callMultiple(r)
    })
}

function callMultiple(r) {
  r.get('/v1/complex/multiple')
    .expect('Content-Type', 'application/json')
    .expect(200)
    .expect(res => {
      if (!res.body.where.city || !res.body.where.country) {
        throw new Error('there is no where city/country')
      }
      if (!res.body.openWeather.summary || !res.body.openWeather.temperature
        || !res.body.openWeather.humidity || !res.body.openWeather.pressure) {
        throw new Error('openWeather is bad defined')
      }
      if (!res.body.darkSky.summary || !res.body.darkSky.temperature
        || !res.body.darkSky.humidity || !res.body.darkSky.pressure) {
        throw new Error('darkSky is bad defined')
      }
    })
    .end((err) => {
      if (err) {
        throw err
      }
      app.close()
    })
}
