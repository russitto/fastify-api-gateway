const fetch = require('node-fetch')
const config = require('../config.json')

module.exports = {
  checkdb: {
    url: '/util/checkdb',
    method: 'GET',
    schema: {
      response: {
        '200': {
          description: 'Succesful response',
          type: 'object',
          properties: {
            alive: {
              type: 'boolean'
            }
          }
        }
      }
    },
    handler: checkdb
  },
  ping: {
    url: '/util/ping',
    method: 'GET',
    schema: {
      response: {
        '200': {
          description: 'Succesful response',
          type: 'object',
          properties: {
            alive: {
              type: 'boolean'
            }
          }
        }
      }
    },
    handler: ping
  },
  ehlo: {
    url: '/util/ehlo',
    method: 'POST',
    schema: {
      body: {
        type: 'object',
        properties: {
          service: {
            type: 'string'
          },
          ipAddress: {
            type: 'string',
            format: 'ipv4'
          },
          userAgent: {
            type: 'string'
          }
        },
        required: ['service', 'ipAddress', 'userAgent']
      },
      response: {
        '200': {
          description: 'Succesful response',
          type: 'object',
          properties: {
            userRegion: {
              type: 'string'
            },
            region: {
              type: 'string'
            },
            regionName: {
              type: 'string'
            },
            ipAddress: {
              type: 'string',
              format: 'ipv4'
            }
          }
        }
      }
    },
    handler: ehlo
  }
}

function checkdb(request, reply) {
  let response
  return fetch(config.servers.util + 'health/check/db')
    .then(res => {
      response = res
      return res.json()
    })
    .then(json => {
      if (response.status != 200) {
        request.log.error(response.status, json)
      }
      return reply.code(response.status).send(json)
    })
    .catch(e => {
      request.log.error(e)
      reply.code(response.status).send('Error ' + e)
    })
}

function ping(request, reply) {
  let response
  return fetch(config.servers.util + 'health/ping')
    .then(res => {
      response = res
      return res.json()
    })
    .then(json => {
      if (response.status != 200) {
        request.log.error(response.status, json)
      }
      return reply.code(response.status).send(json)
    })
    .catch(e => {
      request.log.error(e)
      reply.code(response.status).send('Error ' + e)
    })
}

function ehlo(request, reply) {
  let response
  fetch(config.servers.util + 'v1/api/misc/ehlo', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(request.body)
  })
}
