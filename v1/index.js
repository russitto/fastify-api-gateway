const hello = require('./hello')
const ping = require('./ping')
const Compl = require('./complex')
const utils = require('../utils')
const extUser = require('./external/user')

module.exports = function (fastify, opts, next) {
  const complex = Compl(fastify)

  fastify.get('/hello', hello)
  fastify.get('/ping', ping)

  fastify.route(complex.hello) // /complex/hello
  fastify.route(complex.auth)  // /complex/auth
  fastify.route(complex.multiple)  // /complex/multiple

  extUser.routes.forEach(route => {
    fastify.route(utils.request(route.external, route.spec))
  })

  next()
}
