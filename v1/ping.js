module.exports = function (request, reply) {
  let ip = request.req.connection.remoteAddress
  if (request.headers['x-forwarded']) {
    ip = request.headers['x-forwarded']
  }
  if (request.headers['x-forwarded-for']) {
    ip = request.headers['x-forwarded-for']
  }
  reply.send(ip)
}
