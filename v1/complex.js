const fetch = require('node-fetch')

module.exports = function (fastify) {
  async function hello(request) {
    return {
      hello: `${request.body.name} @ ${fastify.jwtDecoded.operator}`
    }
  }

  async function auth(request) {
    return fastify.jwt.sign(request.body)
  }

  async function multiple(request, reply) {
    const idOpenWeather = '602d95436847bca086da598c19326474'
    const idDarkSky = '92078c6a20eb0a701d6725e189432424'

    let response, where
    try {
      response = await fetch('http://ip-api.com/json')
      // response = await fetch(`http://www.qubit.tv/asdfasdfasdfasdf${idDarkSky}`)
      where = await response.json()
    } catch(e) {
      reply.code(response.status)
      return e.message
    }

    response = await (Promise.all([
      fetch('http://api.openweathermap.org/data/2.5/weather'
        + `?lat=${where.lat}&lon=${where.lon}&units=metric&APPID=${idOpenWeather}`),
      fetch(`https://api.darksky.net/forecast/${idDarkSky}/${where.lat},${where.lon}?units=ca`)
      // fetch(`http://www.qubit.tv/asdfasdfasdfasdf${idDarkSky}`)
    ])
      .then(ress => {
        if (ress[0].status != '200') {
          fastify.log.error(ress[0].status, ress[0].url)
          return reply.code(ress[0].status).send(ress[0].statusText)
        }
        if (ress[1].status != '200') {
          fastify.log.error(ress[1].status, ress[1].url)
          return reply.code(ress[1].status).send(ress[1].statusText)
        }
        return Promise.all([ress[0].json(), ress[1].json()])
      })
      .catch(e => {
        fastify.log.error(e)
        return reply.code(401).send('Error', e.message)
      }))

    let openWeather = response[0]
    const darkSky     = response[1].currently
    const summary = openWeather.weather[0]? openWeather.weather[0].description: ''
    openWeather = {
      summary ,
      temperature: openWeather.main.temp,
      pressure: openWeather.main.pressure,
      humidity: openWeather.main.humidity
    }

    return { where, openWeather, darkSky }
  }

  return {
    hello: {
      url: '/complex/hello',
      method: 'POST',
      beforeHandler: fastify.auth([fastify.verifyJWT]),
      security: [{Bearer: []}],
      schema: {
        body: {
          type: 'object',
          properties: {
            name: { type: 'string' }
            //, someOtherKey: { type: 'number' }
          },
          required: ['name']
        },
        response: {
          200: {
            description: 'Succesful response',
            type: 'object',
            properties: {
              hello: {
                type: 'string'
              }
            }
          }
        }
      },
      // eslint-disable-next-line
      handler: hello
    },
    auth: {
      url: '/complex/auth',
      method: 'POST',
      schema: {
        body: {
          type: 'object',
          properties: {
            operator: { type: 'string' }
            //, someOtherKey: { type: 'number' }
          },
          required: ['operator']
        },
        response: {
          200: {
            description: 'Succesful response token',
            type: 'string',
          }
        }
      },
      handler: auth
    },
    multiple: {
      url: '/complex/multiple',
      method: 'GET',
      schema: {
        response: {
          200: {
            description: 'Succesful location and weather',
            type: 'object',
            properties: {
              where: {
                type: 'object',
                properties: {
                  city: {
                    type: 'string'
                  },
                  country: {
                    type: 'string'
                  }
                }
              },
              openWeather: {
                type: 'object',
                properties: {
                  summary: {
                    type: 'string'
                  },
                  temperature: {
                    type: 'string'
                  },
                  humidity: {
                    type: 'string'
                  },
                  pressure: {
                    type: 'string'
                  }
                }
              },
              darkSky: {
                type: 'object',
                properties: {
                  summary: {
                    type: 'string'
                  },
                  temperature: {
                    type: 'string'
                  },
                  humidity: {
                    type: 'string'
                  },
                  pressure: {
                    type: 'string'
                  }
                }
              }
            }
          }
        }
      },
      handler: multiple
    }
  }
}
