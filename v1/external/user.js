const config = require('../../config.json')

const userDataProps = {
  username: {
    type: 'string'
  },
  firstName: {
    type: 'string'
  },
  lastName: {
    type: 'string'
  },
  email: {
    type: 'string',
    format: 'email'
  },
  phone: {
    type: 'string'
  }
}

const userDataPlusIdProps = Object.assign({
  id: {
    type: 'string',
    format: 'uuid'
  }
}, userDataProps)

const userDataPlusPassProps = Object.assign(userDataProps, {
  password: {
    type: 'string'
  }
})

const userProps = Object.assign(userDataPlusIdProps, {
  region: {
    type: 'string'
  },
  last_login: {
    type: 'string'
  },
  hash: {
    type: 'string'
  },
  requireNewPassword: {
    type: 'boolean'
  },
  validatedUser: {
    type: 'boolean'
  },
  profiles: {
    type: 'array',
    items: {
      type: 'object',
      properties: {
        id: {
          type: 'string',
          format: 'uuid'
        },
        name: {
          type: 'string',
        },
        gender: {
          type: 'integer',
        },
        image: {
          type: 'string',
        },
        restriction: {
          type: 'integer',
        },
        type: {
          type: 'string',
        }
      }
    }
  }
})

const dateProps = {
  date: {
    type: 'string'
  },
  timezone_type: {
    type: 'integer'
  },
  timezone: {
    type: 'string'
  }
}

const deviceProps = {
  deviceId: {
    type: 'string',
    format: 'uuid'
  },
  userId: {
    type: 'string',
    format: 'uuid'
  },
  userAgent: { type: 'string' },
  active: { type: 'boolean' },
  created: {
    type: 'object',
    properties: dateProps
  },
  updated: {
    type: 'object',
    properties: dateProps
  },
  lastLogin: {
    type: 'object',
    properties: dateProps
  },
  platform: { type: 'string' },
  platformVersion: { type: 'string' },
  browserName: { type: 'string' },
  majorVersion: { type: 'string' },
  version: { type: 'string' },
  isMobile: { type: 'boolean' },
  isTablet: { type: 'boolean' }
}

const historyProps = {
  userId: {
    type: 'string',
    format: 'uuid'
  },
  deviceId: {
    type: 'string',
    format: 'uuid'
  },
  ipAddress: {
    type: 'string',
    format: 'ipv4'
  },
  state: {type: 'boolean'},
  created: {
    type: 'object',
    properties: dateProps
  }
}

module.exports = {
  defs: {
    user: userProps,
    date: dateProps,
    device: deviceProps
  },
  routes: [
    {
      external: config.servers.util + 'v1/api/misc/ehlo',
      spec: {
        url: '/util/ehlo',
        method: 'POST',
        schema: {
          tags: ['Util'],
          summary: 'Hello/Helo, first contact',
          body: {
            type: 'object',
            properties: {
              service: {
                type: 'string'
              },
              ipAddress: {
                type: 'string',
                format: 'ipv4'
              },
              userAgent: {
                type: 'string'
              }
            },
            required: ['service', 'ipAddress', 'userAgent']
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'object',
              properties: {
                userRegion: {
                  type: 'string'
                },
                region: {
                  type: 'string'
                },
                regionName: {
                  type: 'string'
                },
                ipAddress: {
                  type: 'string',
                  format: 'ipv4'
                }
              }
            }
          }
        }
      }
    },{
      external: config.servers.user + 'v1/api/auth/login',
      spec: {
        url: '/auth/login',
        method: 'POST',
        schema: {
          tags: ['Auth'],
          summary: 'Login a user',
          body: {
            type: 'object',
            properties: {
              username: {
                type: 'string'
              },
              password: {
                type: 'string'
              }
            },
            required: ['username', 'password']
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'object',
              properties: userProps
            }
          }
        }
      }
    }, {
      external: config.servers.user + 'v1/api/auth/autologin',
      spec: {
        url: '/auth/autologin',
        method: 'POST',
        schema: {
          tags: ['Auth'],
          summary: 'Autologin a user',
          body: {
            type: 'object',
            properties: {
              hash: {
                type: 'string'
              }
            },
            required: ['hash']
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'object',
              properties: userProps
            }
          }
        }
      }
    }, {
      external: config.servers.user + 'v1/api/auth/changePassword', 
      spec: {
        url: '/auth/changePassword',
        method: 'POST',
        schema: {
          tags: ['Auth'],
          summary: 'Change user password',
          body: {
            type: 'object',
            properties: {
              id: {
                type: 'string',
                format: 'uuid'
              },
              current: {
                type: 'string'
              },
              'new': {
                type: 'string'
              }
            },
            required: ['id', 'current', 'new']
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'object',
              properties: userProps
            }
          }
        }
      }
    }, {
      external: config.servers.user + 'v1/api/auth/resetPassword', 
      spec: {
        url: '/auth/resetPassword',
        method: 'POST',
        schema: {
          tags: ['Auth'],
          summary: 'Reset user password',
          body: {
            type: 'object',
            properties: {
              id: {
                type: 'string',
                format: 'uuid'
              }
            },
            required: ['id']
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'object',
              properties: userProps
            }
          }
        }
      }
    }, {
      external: config.servers.user + 'v1/api/auth/extra/devices',
      spec: {
        url: '/auth/extra/devices',
        method: 'GET',
        schema: {
          tags: ['Auth'],
          summary: 'Return a list of devices linked to user',
          userId: {
            type: 'string',
            format: 'uuid'
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'object',
              properties: {
                devices: {
                  type: 'array',
                  items: {
                    type: 'object',
                    properties: deviceProps
                  }
                }
              }
            }
          }
        }
      }
    }, {
      external: config.servers.user + 'v1/api/auth/extra/device/disable',
      spec: {
        url: '/auth/extra/device/disable',
        method: 'POST',
        schema: {
          tags: ['Auth'],
          summary: 'Disable an user device',
          body: {
            type: 'object',
            properties: {
              userId: {
                type: 'string',
                format: 'uuid'
              },
              deviceId: {
                type: 'string',
                format: 'uuid'
              }
            },
            required: ['userId', 'deviceId']
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'object',
              properties: {
                devices: {
                  type: 'array',
                  items: {
                    type: 'object',
                    properties: deviceProps
                  }
                }
              }
            }
          }
        }
      }
    }, {
      external: config.servers.user + 'v1/api/auth/extra/loginHistory',
      spec: {
        url: '/auth/extra/loginHistory',
        method: 'GET',
        schema: {
          tags: ['Auth'],
          summary: 'Return user login history',
          userId: {
            type: 'string',
            format: 'uuid'
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'object',
              properties: {
                history: {
                  type: 'array',
                  items: {
                    type: 'object',
                    properties: historyProps
                  }
                }
              }
            }
          }
        }
      }
    }, {
      external: config.servers.user + 'v1/api/user/check',
      spec: {
        url: '/user/check',
        method: 'POST',
        schema: {
          tags: ['User'],
          summary: 'Check if the username is available',
          body: {
            type: 'object',
            properties: {
              username: { type: 'string' }
            },
            required: ['username']
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'boolean'
            }
          }
        }
      }
    }, {
      external: config.servers.user + 'v1/api/user/get',
      spec: {
        url: '/user/get',
        method: 'GET',
        schema: {
          tags: ['User'],
          summary: 'Retrieve a user',
          id: {
            type: 'string',
            format: 'uuid'
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'object',
              properties: userProps
            }
          }
        }
      }
    }, {
      external: config.servers.user + 'v1/api/user/new',
      spec: {
        url: '/user/new',
        method: 'POST',
        schema: {
          tags: ['User'],
          summary: 'Create new user',
          body: {
            type: 'object',
            properties: userDataPlusPassProps,
            required: ['username', 'firstName', 'lastName', 'password']
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'object',
              properties: userProps
            }
          }
        }
      }
    }, {
      external: config.servers.user + 'v1/api/user/modify',
      spec: {
        url: '/user/modify',
        method: 'POST',
        schema: {
          tags: ['User'],
          summary: 'Modify an user',
          body: {
            type: 'object',
            properties: userDataPlusIdProps,
            required: ['id']
          },
          response: {
            '200': {
              description: 'Succesful response',
              type: 'object',
              properties: userProps
            }
          }
        }
      }
    }
  ]
}
